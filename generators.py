# Define a gen_range function that works like the builtin range function. It should yield a number in each iteration

def gen_range(start, stop, step):
    iteration = start
    while iteration < stop:
        yield iteration
        iteration += step


# Define a zip function that works similar to the builtin zip function. (Unlike the builtin zip, assume that all iterables are of the same size)

def gen_zip(*iterables):
    for index in range(len(iterables[0])):
        result = []
        for elem in iterables:
            result.append(elem[index])
        yield tuple(result)
