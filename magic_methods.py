'''         Warmup         '''

#Add two integers using a suitable magic method

(2).__add__(5)

# Find the length of a list using a suitable magic method

[1,3,6].__len__()

# Find the length of a string using a suitable magic method

"abcd".__len__()

# Add two lists by calling a magic method on the first list

[1,2,3].__add__([5,6,8])



#### Sortable and comparable classes:

# Create a class, BankAccount as below

class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)

    def __lt__(self, other):
        return self.balance < other.balance
    
    def __eq__(self, other):
        return self.id == other.id



# Create a list of BankAccounts
import random

accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(100)]


# Sort accounts by balance, using suitable magic methods

sorted_accounts = sorted(accounts)


# Compare whether two bank accounts are equal. Two bank accounts are equal if they have the same id and balance
a1 = BankAccount(id=1, balance=100)
a2 = BankAccount(id=1, balance=100)
a3 = BankAccount(id=2, balance=100)

a1 == a2
a1 == a3

'''   Custom Iterator  '''

#Define a class Bank, which contains BankAccounts


class Bank:
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)

    def __iter__(self):
        self.id = 0
        return self

    def __next__(self):
        if self.id < self.accounts.__len__():
            result = self.accounts[self.id]
            self.id+=1
            return result
        else:
            raise StopIteration


#Use a magic method so that you can iterate through the accounts in the bank using the below syntax



bank = Bank()

for i in range(100):
    bank.add_account(BankAccount(id=i, balance=i * 100))

for account in bank:
    print(account)