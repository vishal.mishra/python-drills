
import math
import time

'''  
Decorate both the functions with a single decorator, called timer,
that prints how much time is taken to evaluate the above functions.

'''

def timer(function):
    def inner(arg):
        start = time.time()
        result = function(arg)
        end = time.time()
        print('Time Elapsed is {}\nResult is {}'.format((end - start), result))
    return inner


@timer
def sqrt(x):
    return math.sqrt(x)


@timer
def square(x):
    return x * x


'''
Define a decorator function that prints how much time is 
taken to evaluate any function.
'''


def generic_decorators(function):
    def inner(*args, **kwargs):
        start = time.time()
        result = function(*args, **kwargs)
        end = time.time()
        print(function.__name__)
        print('Time Elapsed is {}\nResult is {}'.format((end - start), result))
    return inner


@generic_decorators
def add(x, y):
    return x + y


@generic_decorators
def add3(x, y, z):
    return x + y + z


@generic_decorators
def add_any(*args):
    return sum(args)


@generic_decorators
def abs_add_any(*args, **kwargs):
    total = sum(args)
    if kwargs.get('abs') is True:
        return abs(total)
    return total

'''
Define a memoize decorator, that caches the result of any function which 
takes one argument. Decorate fib(n) with memoize, and notice 
that each fib(n) is computed only for each n.
'''


def memoize(function):
    cache_mem = {}
    def inner(arg):
        start = time.time()
        if cache_mem.get(arg):
            end = time.time()
            print('Time taken using the cache memory is : {}'.format(end - start))
            return cache_mem.get(arg)
        result = function(arg)
        cache_mem[arg] = result
        end = time.time()
        print('Time taken to execute the function is : {}'.format(end - start))
        return result
    return inner



@memoize
def fib(n):
    print("Computing fib({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)


'''
Use the lru_cache decorator from the functools module to memoize 
the below function:
'''

from functools import lru_cache

@lru_cache(maxsize = 2)
def factorial(n):
    print("Computing factorial({})".format(n))
    fact = 1
    for i in range(1, n + 1):
        fact *= i
    return fact
